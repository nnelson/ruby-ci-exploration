#! /usr/bin/env ruby
# frozen_string_literal: true

require 'concurrent/atomics'

atomic_reference = Concurrent::AtomicReference.new(0)
atomic_reference.update { |v| v + 1 }
puts "atomic_reference.value: #{atomic_reference.value}"
